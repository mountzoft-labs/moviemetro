package com.mountzoft.moviemetro.model

import java.io.Serializable

/**
 * Created by <Jithin/Jude> on 26,October,2019.
 * jithin.jude68@gmail.com
 */

data class MovieReviewModel(
    var posterUrl: String = "",
    var title: String = "",
    var releaseDate: String = "",
    var genre: String = "",
    var editorsRating: String = "",
    var numOfRecommendation: Map<String, NumOfRecommendation> = HashMap(),
    var oneLineReview: String = "",
    var review: String = "",
    var director: String = "",
    var writer: String = "",
    var cast: String = "",
    var reviewPublishTimeStamp: String = ""
) : Serializable

data class NumOfRecommendation(
    var deviceId: String = "",
    var recommendation: String = ""
) : Serializable

