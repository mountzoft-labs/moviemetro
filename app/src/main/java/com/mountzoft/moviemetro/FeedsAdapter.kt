package com.mountzoft.moviemetro

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mountzoft.moviemetro.model.MovieReviewModel
import com.mountzoft.moviemetro.model.NumOfRecommendation
import kotlinx.android.synthetic.main.feed_item.view.*

/**
 * Created by <Jithin/Jude> on 25,October,2019.
 * jithin.jude68@gmail.com
 */

class FeedsAdapter (
    private val context: Context,
    private val movieReviewList: MutableList<MovieReviewModel>
): RecyclerView.Adapter<FeedsAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedsAdapter.ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.feed_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movieReviewList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val positiveRecommendation = getNumOfPositiveRecommendation(movieReviewList[position].numOfRecommendation)

        holder.textViewMovieTitle.text = movieReviewList[position].title
        holder.textViewMovieOneLineReview.text = movieReviewList[position].oneLineReview
        holder.textViewEditorsRating.text = movieReviewList[position].editorsRating+"/10"

        if(movieReviewList[position].numOfRecommendation.size-1 == 0){
            holder.textViewPeoplesRecommendation.text = "No recommendations yet."
        }else{
            val recommendationText = ""+positiveRecommendation+" out of "+(movieReviewList[position].numOfRecommendation.size-1)+" people recommending this movie."
            holder.textViewPeoplesRecommendation.text = recommendationText
        }

        Glide.with(context)
            .load(movieReviewList[position].posterUrl)
            .placeholder(R.drawable.loading_placeholder)
            .into(holder.imageViewMoviePoster)

        holder.itemFeed.setOnClickListener {
            val intent = Intent(context, ReviewActivity::class.java)
            intent.putExtra("currentReview", movieReviewList[position])
            intent.putExtra("totalRecommendation",(movieReviewList[position].numOfRecommendation.size-1))
            intent.putExtra("positiveRecommendation",positiveRecommendation)
            context.startActivity(intent)
        }
    }

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view){
        var itemFeed = view.feedItem
        var textViewMovieTitle = view.tvMovieTitle
        var textViewMovieOneLineReview = view.tvMovieOneLineReview
        var textViewEditorsRating = view.tvEditorsRating
        var textViewPeoplesRecommendation = view.tvPeoplesRecommendation
        var imageViewMoviePoster = view.ivMoviePoster
    }

    fun getNumOfPositiveRecommendation(recommendation: Map<String, NumOfRecommendation>): Int{
        var mRecommendation = 0
        for(item in recommendation){
            if(item.value.recommendation == "1"){
                mRecommendation++
            }
        }
        return mRecommendation
    }
}