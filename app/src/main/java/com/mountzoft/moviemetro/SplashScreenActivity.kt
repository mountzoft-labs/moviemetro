package com.mountzoft.moviemetro

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.lang.Thread.sleep

/**
 * Created by <Jithin/Jude> on 24,October,2019.
 * jithin.jude68@gmail.com
 */

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ShowSplash().execute()
    }

    override fun onPause() {
        super.onPause()
        finish()
    }

    override fun onBackPressed() {}

    //=========================================AsyncTask================================================
    private inner class ShowSplash : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void?): String? {

            try {
                sleep(1000)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            val intent = Intent(this@SplashScreenActivity, FeedsActivity::class.java)
            startActivity(intent)
        }

    }
    //=========================================AsyncTask================================================
}