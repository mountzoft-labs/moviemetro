package com.mountzoft.moviemetro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        supportActionBar?.title = "About"
        supportActionBar?.elevation = 0.0f
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val textPrivacyPolicy =
            Html.fromHtml("<a href='https://movie-metro-app.firebaseapp.com/movie_metro_app_privacy_policy.html'>Privacy Policy</a>")

        tvPrivacyPolicy.text = textPrivacyPolicy
        tvPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()

        val textTc =
            Html.fromHtml("<a href='https://movie-metro-app.firebaseapp.com/movie_metro_app_terms_and_conditions.html'>T & C</a>")
        tvTc.text = textTc

        tvTc.movementMethod = LinkMovementMethod.getInstance()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
