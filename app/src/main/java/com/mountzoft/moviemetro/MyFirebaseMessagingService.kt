package com.mountzoft.moviemetro

import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Created by <Jithin/Jude> on 28,October,2019.
 * jithin.jude68@gmail.com
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {
    val TAG = "Service"

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)

        val database = FirebaseDatabase.getInstance()
        val databaseReference = database.getReference("fcmToken")

        databaseReference.child(getDeviceId()).setValue(newToken)

        Log.d("NEW_TOKENT============",newToken)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.

        /*val handler = Handler(Looper.getMainLooper())
        handler.post(Runnable { Toast.makeText(applicationContext,"Received: "+remoteMessage.notification!!.title ,Toast.LENGTH_SHORT).show() })*/

        Log.d(TAG, "Title: " + remoteMessage.notification!!.title)
        Log.d(TAG, "Notification Message Body: " + remoteMessage.notification!!.body!!)
    }

    fun getDeviceId(): String{
        return Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID);
    }
}