package com.mountzoft.moviemetro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_feeds.*
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import com.mountzoft.moviemetro.model.MovieReviewModel
import kotlinx.android.synthetic.main.activity_feeds.toolbar


class FeedsActivity : AppCompatActivity() {

    private  var movieReviewList: MutableList<MovieReviewModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feeds)

        setSupportActionBar(toolbar)

        FirebaseMessaging.getInstance().isAutoInitEnabled = true

        rvFeeds.layoutManager = LinearLayoutManager(applicationContext)
        getMovieReviews()

        //TODO: pagination if the users needs...
/*        rvFeeds.addOnScrollListener(object:RecyclerView.OnScrollListener() {
            override fun onScrolled(@NonNull recyclerView:RecyclerView, dx:Int, dy:Int) {
                super.onScrolled(recyclerView, dx, dy)
                if( !recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)){
                    Toast.makeText(applicationContext,"Scroll Ends",Toast.LENGTH_SHORT).show()
                }
            }
        })*/
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_feeds_activity_appbar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == R.id.about) {
            val intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    fun getMovieReviews(){
        progressBar.visibility = View.VISIBLE
        FirebaseDatabase.getInstance().getReference("reviews").orderByChild("reviewPublishTimeStamp").limitToLast(15).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                movieReviewList.clear()
                for (issueSnapshot in dataSnapshot.children) {
                    val issue = issueSnapshot.getValue<MovieReviewModel>(
                        MovieReviewModel::class.java)
                    movieReviewList.add(issue!!)

                }

                if(rvFeeds.adapter == null){
                    rvFeeds.adapter = FeedsAdapter(this@FeedsActivity, movieReviewList.asReversed())
                }else{
                    (rvFeeds.adapter as FeedsAdapter).notifyDataSetChanged()
                }
                progressBar.visibility = View.GONE
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }
}
