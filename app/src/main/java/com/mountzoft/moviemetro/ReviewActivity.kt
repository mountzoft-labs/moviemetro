package com.mountzoft.moviemetro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.bumptech.glide.Glide
import com.google.firebase.database.FirebaseDatabase
import com.mountzoft.moviemetro.model.MovieReviewModel
import com.mountzoft.moviemetro.model.NumOfRecommendation
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.review_content.*

class ReviewActivity : AppCompatActivity() {

    var database = FirebaseDatabase.getInstance()
    var databaseReference = database.getReference("reviews")

    lateinit var currentReview: MovieReviewModel
    var totalRecommendation = 0
    var positiveRecommendation = 0

    var askForRecommendation = true
    lateinit var recommendationMenu: Menu
    var userRecommendation = ""

    var localTotalRecommendation = 0
    var localPositiveRecommendation = 0
    var localUserRecommendation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        currentReview = intent?.extras!!.get("currentReview") as MovieReviewModel
        totalRecommendation = intent?.extras!!.getInt("totalRecommendation")
        positiveRecommendation = intent?.extras!!.getInt("positiveRecommendation")

        Glide.with(applicationContext)
            .load(currentReview.posterUrl)
            .placeholder(R.drawable.loading_placeholder)
            .into(ivAppbarPoster)
        tvMovieTitle.text = currentReview.title
        tvReleasedate.text = currentReview.releaseDate
        tvGenre.text = currentReview.genre
        tvEditorsRating.text = currentReview.editorsRating+"/10"
        tvPeoplesRecommendation.text = ""+positiveRecommendation+"/"+totalRecommendation
        tvReview.text = currentReview.review
        tvDirector.text = currentReview.director
        tvWriter.text = currentReview.writer
        tvCast.text = currentReview.cast
        nestedScrollView?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->

            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight && askForRecommendation) {
                showMovieRecommendDialog()
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_review_activity_appbar, menu)
        recommendationMenu = menu
        checkRecommendation()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == R.id.recommendButton) {
            showMovieRecommendDialog()
        }

        if (id == R.id.shareButton) {
            shareReview()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showMovieRecommendDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Movie recommendation")
        builder.setMessage("Would you like to recommend this movie to a friend?")

        val deviceId = getDeviceId()

        builder.setPositiveButton("Yes"){dialogInterface, which ->
            databaseReference
                .child(currentReview.reviewPublishTimeStamp)
                .child("numOfRecommendation")
                .child(deviceId)
                .setValue(NumOfRecommendation(deviceId, "1"))

            if(!askForRecommendation){
                updateUserRecommendation("increase")
            }else{
                updateIntialUserRecommendation("increase")
            }

            askForRecommendation = false
            changeRecommendationIcon("yes")
            showToast()
        }

        builder.setNegativeButton("No"){dialogInterface, which ->
            databaseReference
                .child(currentReview.reviewPublishTimeStamp)
                .child("numOfRecommendation")
                .child(deviceId)
                .setValue(NumOfRecommendation(deviceId, "0"))

            if(!askForRecommendation){
                updateUserRecommendation("decrease")
            }else{
                updateIntialUserRecommendation("decrease")
            }

            askForRecommendation = false
            changeRecommendationIcon("no")
            showToast()
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    fun shareReview(){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, getDataForShare())
        startActivity(Intent.createChooser(shareIntent,"Share "+currentReview.title+" review with your friends!"))
    }

    fun getDataForShare(): String{
        val text = "#Review:\n\n"+currentReview.title+ "\n=====>\n\n"+currentReview.review+"\n\n"+
                "Get latest Malayalam movie reviews:\n"+"http://bit.ly/MovieMetroApp-GetLatestMalayalamMovieReviews"
        return text
    }

    fun showToast(){
        val toast = Toast.makeText(
            applicationContext,
            "Thanks for helping us to find the best movies.", Toast.LENGTH_LONG
        )
        toast.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL, 0, -200)
        toast.show()
    }

    fun getDeviceId(): String{
        return Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun checkRecommendation(){
        for (item in currentReview.numOfRecommendation){
            if(item.value.deviceId == getDeviceId()){
                askForRecommendation = false
                if(item.value.recommendation == "1"){
                    changeRecommendationIcon("yes")
                }else{
                    changeRecommendationIcon("no")
                }
            }
        }

        if(!askForRecommendation){
            userRecommendation = currentReview.numOfRecommendation[getDeviceId()]?.recommendation!!
        }

        localTotalRecommendation = totalRecommendation
        localPositiveRecommendation = positiveRecommendation
        localUserRecommendation = userRecommendation
    }

    fun changeRecommendationIcon(recommendation: String){
        if(recommendation == "yes"){
            recommendationMenu.getItem(0).icon = ContextCompat.getDrawable(this, R.drawable.ic_up_vote_white_24dp)
        }else{
            recommendationMenu.getItem(0).icon = ContextCompat.getDrawable(this, R.drawable.ic_down_vote_white_24dp)
        }
    }

    fun updateUserRecommendation(update: String){
        if(update == "increase" && (localUserRecommendation == "0")){
            localUserRecommendation = "1"
            tvPeoplesRecommendation.text = ""+(++localPositiveRecommendation)+"/"+(localTotalRecommendation)
        }else if(update == "decrease" && (localUserRecommendation == "1")){
            localUserRecommendation = "0"
            tvPeoplesRecommendation.text = ""+(--localPositiveRecommendation)+"/"+(localTotalRecommendation)
        }
    }

    fun updateIntialUserRecommendation(update: String){
        if(update == "increase"){
            localUserRecommendation = "1"
            tvPeoplesRecommendation.text = ""+(++localPositiveRecommendation)+"/"+(++localTotalRecommendation)
        }else if(update == "decrease"){
            localUserRecommendation = "0"
            tvPeoplesRecommendation.text = ""+(--localPositiveRecommendation)+"/"+(--localTotalRecommendation)
        }
    }
}
